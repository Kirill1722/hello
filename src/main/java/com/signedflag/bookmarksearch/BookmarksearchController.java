package com.signedflag.bookmarksearch;

import com.signedflag.account.AccountRepository;
import com.signedflag.bookmark.Bookmark;
import com.signedflag.bookmark.BookmarkRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("{userId}/bookmarksearch")
public class BookmarksearchController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookmarksearchController.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BookmarkRepository bookmarkRepository;

    @RequestMapping(method = RequestMethod.POST)
    public List<Bookmark> searchByQuery(@PathVariable String userId, @RequestBody List<String> keywords) {
        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        List<Bookmark> result = new ArrayList<>();
        for (Bookmark bookmark : bookmarks) {
            String url = bookmark.getUrl();
            for (String keyword : keywords) {
                if (url.contains(keyword)) {
                    result.add(bookmark);

                    continue;
                }
            }
        }
        return result;
    }

}
