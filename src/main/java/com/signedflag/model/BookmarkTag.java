package com.signedflag.model;

import com.signedflag.account.Account;
import com.signedflag.bookmark.Bookmark;

import javax.persistence.*;

@Entity
@Table(name = "bookmark_tag")
public class BookmarkTag {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name="fk_benutzer_id")
    private Account account;

    @OneToOne
    @JoinColumn(name="fk_bookmark_id")
    private Bookmark bookmark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Bookmark getBookmark() {
        return bookmark;
    }

    public void setBookmark(Bookmark bookmark) {
        this.bookmark = bookmark;
    }

}
