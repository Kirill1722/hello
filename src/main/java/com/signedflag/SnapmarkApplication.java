package com.signedflag;

import com.signedflag.account.Account;
import com.signedflag.bookmark.Bookmark;
import com.signedflag.model.BookmarkTag;
import com.signedflag.bookmark.BookmarkRepository;
import com.signedflag.account.AccountRepository;
import com.signedflag.persistence.BookmarkTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SnapmarkApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SnapmarkApplication.class, args);
    }

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BookmarkRepository bookmarkRepository;

    @Autowired
    private BookmarkTagRepository bookmarkTagRepository;

    @PostConstruct
    public void initTestData() {
        try {
            addUser1();
            addUser2();
        } catch (Exception e) {
            //e.printStackTrace();
        }

    }

    private void addUser1() {
        Account account = new Account();
        account.setUsername("TestUser01");
        accountRepository.save(account);

        Bookmark bookmark = new Bookmark();
        bookmark.setAccount(account);
        bookmark.setUrl("http://www.google.ch/");
        bookmarkRepository.save(bookmark);

        BookmarkTag bookmarkTag = new BookmarkTag();
        bookmarkTag.setAccount(account);
        bookmarkTag.setBookmark(bookmark);
        bookmarkTag.setName("Suchmachine");
        bookmarkTagRepository.save(bookmarkTag);
    }

    private void addUser2() {
        Account account = new Account();
        account.setUsername("TestUser02");
        accountRepository.save(account);

        Bookmark bookmark = new Bookmark();
        bookmark.setAccount(account);
        bookmark.setUrl("http://www.gmail.com/");
        bookmarkRepository.save(bookmark);

        BookmarkTag bookmarkTag = new BookmarkTag();
        bookmarkTag.setAccount(account);
        bookmarkTag.setBookmark(bookmark);
        bookmarkTag.setName("Mail");
        bookmarkTagRepository.save(bookmarkTag);
    }

}
