package com.signedflag.util;

public class SnapmarkException extends RuntimeException {

    public SnapmarkException(Exception e) {
        super(e);
    }

}
