package com.signedflag.demo;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {

    @RequestMapping("/helloWorld")
    public @ResponseBody
    String getHelloWorld() {
        return "hello world 10";
    }

    @RequestMapping("/helloWorld/{name}")
    public String getHelloWorld(Map map, @PathVariable String name) {
        map.put("name", name);
        return "hejkllllkkjllo";
    }

}
