FROM kirill1722/all-in-one:1.0.0

EXPOSE 8080

WORKDIR /snapmark

COPY target/snapmark-0.0.1-SNAPSHOT.jar /snapmark/snapmark.jar

CMD ["java","-jar","snapmark.jar"]
