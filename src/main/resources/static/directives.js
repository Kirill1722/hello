snapmark.directive("urlreport", function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/urlreport.html',
        replace: true,
        scope: {
            weatherDay: "="
        }
    }
});